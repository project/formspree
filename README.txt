
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Dependencies
 * Installation
 * Usage
 * Maintainers

INTRODUCTION
------------

Using this module you don't need to setup a mail server or smtp.
Submit contact-forms or webforms trough formspree.io.

FEATURES
--------

- Select forms using a jQuery selector
ex.".webform-client-form" or defaults to ".contact-form"
- Set a mailto value or defaults to site mail.


DEPENDENCIES
------------

jQuery Update


INSTALLATION
------------

Install the module as usual.


USAGE
-----

Set a selector and mailto in /admin/config/system/formspree
Selected form's will be submitted to your email trough formspring.io

MAINTAINERS
-----------

Rudá Maia - https://www.drupal.org/u/rudam
