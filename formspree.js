/**
 * @file
 */

(function ($) {
  'use strict';
  Drupal.behaviors.formspree = {
    attach: function (context, settings) {
      settings.formspree = settings.formspree || Drupal.settings.formspree;
      // Return selected elements.
      var selector = settings.formspree.selector;
      // Return email.
      var email = settings.formspree.email;
      // Status messages.
      var sending = Drupal.t('Sending...');
      var success = Drupal.t('Message sent!');
      var error = Drupal.t('Error! Contact the administrator.');
      $(selector).submit(
        function (e) {
          // If any required input is empty, let contact module handle it.
          var validation;
          $('.contact-form .form-item .required').each(
            function () {
              if ($(this).val() === '') {
                validation = 0;
              }

            }
          );
          if (validation === 0) {

          }

          // Ajaxify the selected forms, serialize the fields, display status messages, and submit the data to formspree@youremail.
          else {
            e.preventDefault();
            $(window).scrollTop(0);
            $.ajax({
              url: 'https://formspree.io/'.concat(email),
              method: 'POST',
              type: 'POST',
              data: $(this).serialize(),
              dataType: 'json',
              beforeSend: function () {
                $('body').find('.alert-warning, .alert-danger').hide();
                $(selector).prepend('<div class="alert alert-warning">'.concat(sending) + '</div>');
              },
              success: function (data) {
                $('body').find('.alert-warning, .alert-danger').hide();
                $(selector).prepend('<div class="alert alert-success">'.concat(success) + '</div>');
                // Prevent submiting multiple times.
                $('#edit-submit, .webform-submit').prop('disabled', true);
              },
              error: function (err) {
                $('body').find('.alert-warning, .alert-danger').hide();
                $(selector).prepend('<div class="alert alert-danger">'.concat(error) + '</div>');
              }
            });
          }
        }
      );
    }
  };
})(jQuery);
