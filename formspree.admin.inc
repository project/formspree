<?php

/**
 * @file
 * Formspree administration pages.
 */

/**
 * Returns with the general configuration form.
 */
function formspree_admin_settings($form, &$form_state) {

  $form['formspree_jquery_selector'] = array(
    '#type' => 'textarea',
    '#title' => t('Apply Formspree to the following elements'),
    '#description' => t('A comma-separated list of jQuery selectors to apply Formspree to, such as <code>.webform-client-form, .contact-form</code>. Defaults to <code>.contact-form</code> to apply Formspree to all Drupal contact forms.'),
    '#default_value' => variable_get('formspree_jquery_selector', '.contact-form'),
  );
  $form['formspree_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('The email address to send the form submissions.'),
    '#default_value' => variable_get('formspree_email', variable_get('site_mail')),
  );

  return system_settings_form($form);
}
